#!/usr/bin/perl
use strict;
use warnings; 
no warnings 'uninitialized';
my $accumulator;
sub equals
{
	return $accumulator;	
}

sub clear
{
	return $accumulator = 0;
}

sub plus
{
	return $accumulator += $_[0];
}

sub minus
{
	return $accumulator -= $_[0];
}

sub mult
{
	return $accumulator *=$_[0];
}

sub over
{
	return $accumulator /=$_[0];
}

sub calc
{
	my ($op, $num) = split ' ',$_[0],2;
	if (lc($op) eq "equals")
	{
		print "OK \n";
		print equals;
	}
	elsif (lc($op) eq "clear")
	{
		print "OK \n";
		print clear;
	}
	elsif (lc($op) eq "plus")
	{
		print "OK \n";
		print plus $num;
	}
	elsif (lc ($op) eq "minus")
	{
		print "OK \n";
		print minus $num;
	}
	elsif (lc ($op) eq "times")
	{
		print "OK \n";
		print mult $num;
	}
	elsif (lc ($op) eq "over")
	{
		print "OK \n";
		print over $num;
	}
	else
	{
		print "Invalid statement \n";
		return 1;
	}
	return 0;
}


my $end = 0;
while (!$end)
{	
	print '>';
	my $arg = <>;
	chomp($arg);
	$end = calc $arg;
	if ($accumulator eq undef)
	{
		print '(undefined)';
	}
	print "\n";
}
