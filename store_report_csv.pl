#!/usr/bin/perl
use strict;
use warnings;
use Scalar::Util qw(looks_like_number);

my $file = 'store_report.csv';
open (my $fh,'<', $file) or die "Can't open file!";
my @lines = readline($fh);

my (%item_cost, %inventory);

foreach my $line(@lines)
{
	  my ($cost, $quantity, $item) = split ",", $line, 3;
	  $item_cost{$item} = $cost;
	  $inventory{$item} = $quantity;
	
}

my $salesf = 'sales_report.sales';
open (my $sh,'<', $salesf) or die "Can't open file!";
my @slines = readline($sh);



my %sold;
foreach my $sline(@slines)
{
	my ($qu, $it) = split ' ', $sline;
	$sold{$it} = $qu;	
}
my $n = 1;
foreach my $item ( keys %sold )
{
  if ( exists $inventory{$item} )
  {
    $inventory{$item} -= $sold{$item};
  }
  else
  {
    warn "*** Sold $sold{$item} of $item, which were not in inventory\n";
  }
  if ($sold{$item} <0)
  {
	print "Inventory is negative at line $n \n";
  }
  $n++;
}

foreach my $item ( sort keys %item_cost )
{
	if (looks_like_number ($item_cost{$item}) && looks_like_number ($inventory{$item}))
	{
		printf "%5.2f %6d %s\n", $item_cost{$item}, $inventory{$item}, $item;
	}
}

